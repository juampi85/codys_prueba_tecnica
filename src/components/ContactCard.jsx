import React, { useState } from 'react';
import '../styles/ContactCard.css';

const ContactCard = ({ contact, onDeleteContact, onUpdateContact }) => {
  const [isEditing, setIsEditing] = useState(false);
  const [editedContact, setEditedContact] = useState(contact);

  const handleDelete = () => {
    onDeleteContact(contact.id);
  };

  const handleToggleEdit = () => {
    setIsEditing(!isEditing);
    setEditedContact(contact); // Restaurar los datos originales al cancelar la edición
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setEditedContact((prevData) => ({ ...prevData, [name]: value }));
  };

  const handleUpdate = () => {
    onUpdateContact(editedContact);
    setIsEditing(false);
  };

  return (
    <div className="container">
      {isEditing ? (
        // lógica para EDITAR los campos que quiera
        <div className='form_editing'>
          <label>
            Foto perfil:
            <input
              type="text"
              name="profilePicture"
              value={editedContact.profilePicture}
              onChange={handleChange}
            />
          </label>
          <label>
            Nombre y Apellido:
            <input
              type="text"
              name="fullName"
              value={editedContact.fullName}
              onChange={handleChange}
            />
          </label>
          <label>
            Correo electrónico:
            <input
              type="email"
              name="email"
              value={editedContact.email}
              onChange={handleChange}
            />
          </label>
          <label>
            Teléfono:
            <input
              type="tel"
              name="phone"
              value={editedContact.phone}
              onChange={handleChange}
            />
          </label>
          <label>
            Fecha de nacimiento:
            <input
              type="date"
              name="dob"
              value={editedContact.dob}
              onChange={handleChange}
            />
          </label>
          <button onClick={handleUpdate}>Guardar cambios</button>
          <button onClick={handleToggleEdit}>Cancelar</button>
        </div>
      ) : (
        // lógica para MOSTRAR los campos de mi contacto
        <div className="card">
          {contact.profilePicture && (
            <img
              className="contact_img"
              src={`data:image/png;base64,${contact.profilePicture}`}
              alt="Profile"
            />
          )}
          <p>Nombre: {contact.fullName}</p>
          <p>Correo electrónico: {contact.email}</p>
          <p>Teléfono: {contact.phone}</p>
          <p>Año de nacimiento: {new Date(contact.dob).getFullYear()}</p>
          <div className='btn_containers'>
            <button onClick={handleToggleEdit} className='edit_btn'>Editar</button>
            <button onClick={handleDelete} className='delete_btn'>Eliminar</button>
          </div>
        </div>
      )}
    </div>
  );
};

export default ContactCard;
