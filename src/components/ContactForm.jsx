import React, { useState } from 'react';
import '../styles/ContactForm.css';

const ContactForm = ({ onCreateContact }) => {
  const [formData, setFormData] = useState({
    profilePicture: '', // ahora la convertí a una cadena de datos base64 para poder visualizarla
    fullName: '',
    email: '',
    phone: '',
    dob: '',
  });

  const handleChange = async (e) => {
    const { name, value, type } = e.target;
    if (type === 'file') {
      const file = e.target.files[0];
      const base64String = await convertToBase64(file);
      setFormData((prevData) => ({ ...prevData, [name]: base64String }));
    } else {
      setFormData((prevData) => ({ ...prevData, [name]: value }));
    }
  };

    const isFormValid = () => {
      // acá es donde controlo si todos los campos se rellenaron con datos --> digamos, la "validación"
      return Object.values(formData).every((field) => field !== '');
    };

  const handleSubmit = (e) => {
    e.preventDefault();
   if (isFormValid()) {
     onCreateContact(formData);
     setFormData({
       profilePicture: '',
       fullName: '',
       email: '',
       phone: '',
       dob: '',
     });
   } else {
     alert('Es necesario rellenar todos los campos antes de guardar.');
   }
  };

  const convertToBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result.split(',')[1]);
      reader.onerror = (error) => reject(error);
    });
  };

  return (
    <form onSubmit={handleSubmit} className="contact_form">
      <label className="form_label">
        Foto de Perfil:
        <input
          type="file"
          name="profilePicture"
          onChange={handleChange}
          accept="image/*"
          className="form_input"
        />
      </label>
      <label className="form_label">
        Nombre y Apellido:
        <input
          type="text"
          name="fullName"
          value={formData.fullName}
          onChange={handleChange}
          className="form_input"
        />
      </label>
      <label className="form_label">
        Correo electrónico:
        <input
          type="email"
          name="email"
          value={formData.email}
          onChange={handleChange}
          className="form_input"
        />
      </label>
      <label className="form_label">
        Teléfono:
        <input
          type="tel"
          name="phone"
          value={formData.phone}
          onChange={handleChange}
          className="form_input"
        />
      </label>
      <label className="form_label">
        Fecha de nacimiento:
        <input
          type="date"
          name="dob"
          value={formData.dob}
          onChange={handleChange}
          className="form_input"
        />
      </label>
      <button type="submit" className="form_btn">
        Guardar
      </button>
    </form>
  );
};

export default ContactForm;
