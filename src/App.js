import React, { useEffect, useState } from 'react';
import ContactForm from './components/ContactForm';
import ContactCard from './components/ContactCard';
import './App.css'

const App = () => {
  const [contacts, setContacts] = useState([]);

  useEffect(() => {
    const storedContacts = JSON.parse(localStorage.getItem('contacts')) || [];
    setContacts(storedContacts);
  }, []);

  const handleCreateContact = (contactData) => {
    const newContact = { id: Date.now(), ...contactData };
    const newContacts = [...contacts, newContact];
    setContacts(newContacts);
    localStorage.setItem('contacts', JSON.stringify(newContacts));
  };

  const handleDeleteContact = (id) => {
    const updatedContacts = contacts.filter((contact) => contact.id !== id);
    setContacts(updatedContacts);
    localStorage.setItem('contacts', JSON.stringify(updatedContacts));
  };

  // acá hago la lógica para poder modificar los contactos
  const handleUpdateContact = (updatedContact) => {
    const updatedContacts = contacts.map((contact) =>
      contact.id === updatedContact.id ? updatedContact : contact
    );
    setContacts(updatedContacts);
    localStorage.setItem('contacts', JSON.stringify(updatedContacts));
  };

  return (
    <div className="app-container">
      <ContactForm onCreateContact={handleCreateContact} />
      {contacts.map((contact) => (
        <ContactCard
          key={contact.id}
          contact={contact}
          onDeleteContact={handleDeleteContact}
          onUpdateContact={handleUpdateContact}
        />
      ))}
    </div>
  );
};

export default App;
